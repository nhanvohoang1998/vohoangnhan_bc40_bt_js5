// Bài 1: Quản lý tuyển sinh
/**
 * inout: Nhập vào các giá trị điểm chuẩn, chọn khu vực, chọn đối tượng, điểm 3 môn
 * handle: Viết hàm tính điểm khu vực và tính điểm đối tướng, viết hàm tổng điểm khi cộng toàn bộ điểm lại. Kiểm tra điểm chuẩn nhập vào và điểm 3 môn có đúng với điều kiện chuẩn đặt ra hay không. Dùng if elese if để lọc các trường hợp có thể xảy ra
 * output: Xuất ra sinh viên đậu hay rớt và tổng điểm 3 môn
 */
function tongDiem3Mon(diemMonThu1, diemMonThu2, diemMonThu3, diemKhuVuc, diemDoiTuong) {
    if (diemMonThu1 == 0 || diemMonThu2 == 0 || diemMonThu3 == 0) {
        return -1;
    } else if (diemMonThu1 < 0 || diemMonThu1 > 10 || diemMonThu2 < 0 || diemMonThu2 > 10 || diemMonThu3 < 0 || diemMonThu3 > 10) {
        return -2;
    } else {
        return (diemMonThu1 + diemMonThu2 + diemMonThu3 + diemKhuVuc + diemDoiTuong);
    }
}

function diemKhuVuc(chonKhuVuc) {
    switch (chonKhuVuc) {
        case 1: {
            return 0;
        }
        case 2: {
            return 2;
        }
        case 3: {
            return 1;
        }
        case 4: {
            return 0.5;
        }
    }
}

function diemDoiTuong(chonDoiTuong) {
    switch (chonDoiTuong) {
        case 1: {
            return 0;
        }
        case 2: {
            return 2.5;
        }
        case 3: {
            return 1.5;
        }
        case 4: {
            return 1;
        }
    }
}

var ketQuaDauRot = document.getElementById('ketQuaDauRot');
ketQuaDauRot.addEventListener("click", function () {
    var diemMonThu1 = document.getElementById("diemMonThu1").value * 1;
    var diemMonThu2 = document.getElementById("diemMonThu2").value * 1;
    var diemMonThu3 = document.getElementById("diemMonThu3").value * 1;
    var diemChuan = document.getElementById("diemChuan").value * 1;
    var chonKhuVuc = document.getElementById("chonKhuVuc").value * 1;
    var chonDoiTuong = document.getElementById("chonDoiTuong").value * 1;
    var ketQua_bt1 = document.getElementById("ketQua_bt1");

    var tinhDiemKhuVuc = diemKhuVuc(chonKhuVuc);
    var tinhDiemDoiTuong = diemDoiTuong(chonDoiTuong);
    var tongDiem3Mon_KV_DT = tongDiem3Mon(diemMonThu1, diemMonThu2, diemMonThu3, tinhDiemKhuVuc, tinhDiemDoiTuong);

    if (diemChuan < 0 || diemChuan > 30) {
        alert("Kiểm tra lại điểm chuẩn")
    } else {
        if (tongDiem3Mon_KV_DT == -2) {
            alert("Kiểm tra lại điểm 3 môn đã nhập")
        } else if (tongDiem3Mon_KV_DT == -1) {
            ketQua_bt1.innerHTML = "Bạn đã rớt. Do có điểm liệt = 0";
            ketQua_bt1.className = "alert alert-danger"
        } else if (tongDiem3Mon_KV_DT >= diemChuan) {
            ketQua_bt1.innerHTML = `Bạn đã đậu. Tổng điểm: ${tongDiem3Mon_KV_DT}`
            ketQua_bt1.className = "alert alert-success"
        } else {
            ketQua_bt1.innerHTML = `Bạn đã rớt. Tổng điểm: ${tongDiem3Mon_KV_DT}`
            ketQua_bt1.className = " alert alert-danger"
        }
    }
});

// Bài 2: Tính tiền điện 
/**
 * input: Nhập vào tên người sử dụng và số kw đã sử dụng
 * handle: Viêt các hàm tính tiền sử dụng cho các trường hợp 50kw đầu, 50kw kế, 100kw kế,... 
 * output: Xuất ra được số tiền cần phải trả
 */
function tien50KwDau(soKw) {
    return (soKw * 500);
}
function tien50KwKe(soKw) {
    return (50 * 500 + (soKw - 50) * 650);
}
function tien100KwKe(soKw) {
    return (50 * 500 + 50 * 650 + (soKw - 100) * 850);
}
function tien150KwKe(soKw) {
    return (50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100);
}
function tienKwConLai(soKw) {
    return (50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300);
}

var tinhTienDien = document.getElementById("tinhTienDien");
tinhTienDien.addEventListener("click", function () {
    var hoTen = document.getElementById("hoTen").value;
    var soKw = document.getElementById("soKw").value * 1;
    var ketQua_bt2 = document.getElementById("ketQua_bt2");
    var tienDien = 0;

    if (soKw <= 0) {
        alert("Số Kw không hợp lệ, vui lòng nhập lại")
    } else {
        if (soKw <= 50) {
            tienDien = tien50KwDau(soKw);
        } else if (soKw <= 100) {
            tienDien = tien50KwKe(soKw);
        } else if (soKw <= 200) {
            tienDien = tien100KwKe(soKw);
        } else if (soKw <= 350) {
            tienDien = tien150KwKe(soKw);
        } else {
            tienDien = tienKwConLai(soKw);
        }
        ketQua_bt2.innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${tienDien.toLocaleString()}`
    }
});